module gitlab.com/codehell/go_base

go 1.13

require (
	github.com/Microsoft/go-winio v0.4.14 // indirect
	github.com/alexedwards/scs/v2 v2.2.0
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.13.1 // indirect
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/gorilla/csrf v1.6.1
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	google.golang.org/appengine v1.6.3 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
