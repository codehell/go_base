package config

// AppConfig configuration for application
type AppConfig struct {
	Key      string
	Database struct {
		Host     string
		Port     string
		User     string
		Password string
		Database string
	}
}
