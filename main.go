package main

import (
	"database/sql"
	"github.com/alexedwards/scs/v2"
	"gitlab.com/codehell/go_base/config"
	"gitlab.com/codehell/go_base/database"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gorilla/csrf"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	appConfig      config.AppConfig
	db             *sql.DB
	sessionManager *scs.SessionManager
)

func init() {
	body, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.Fatal("unable to read file")
	}

	err = yaml.Unmarshal(body, &appConfig)
	if err != nil {
		log.Fatal(err)
	}
	db = database.GetDB(appConfig)
	database.Migrate(db)
	sessionManager = scs.New()
	sessionManager.Lifetime = 24 * time.Hour
}

func main() {
	defer func() {
		err := db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(SetJSONContentType)
	isProduction := os.Getenv("GCP_ENVIRONMENT") == "production"
	csrfOption := csrf.Secure(isProduction)
	csrfMiddleware := csrf.Protect([]byte(appConfig.Key), csrfOption)
	r.Use(csrfMiddleware)

	r.Route("/rest", func(r chi.Router) {
		r.Get("/ping", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("X-CRSF-Token", csrf.Token(r))
		})
		r.Post("/ping", func(w http.ResponseWriter, r *http.Request) {
			APIResponse(w, "pong", "pingResponse", http.StatusOK)
		})
	})

	log.Fatal(http.ListenAndServe(":8080", sessionManager.LoadAndSave(r)))
}

// SetJSONContentType is a middleware that inserts content type json in header
func SetJSONContentType(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
