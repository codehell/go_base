package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"gitlab.com/codehell/go_base/config"
	"log"
)

func GetDB(appConfig config.AppConfig) *sql.DB {
	dataSourceName := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s",
		appConfig.Database.User,
		appConfig.Database.Password,
		appConfig.Database.Host,
		appConfig.Database.Port,
		appConfig.Database.Database,
	)
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("the database was opened")

	err = db.Ping()
	if err != nil {
		log.Fatalf("Error conectando: %v", err)
		return nil
	}
	return db
}

func Migrate(db *sql.DB) {
	driver, _ := mysql.WithInstance(db, &mysql.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://database/migrations",
		"mysql",
		driver,
	)
	defer func() {
		_, _ = m.Close()
	}()
	if err != nil {
		log.Fatal(err)
	}
	err = m.Migrate(2)
	if err != nil {
		log.Println(err)
	}
}
