CREATE TABLE IF NOT EXISTS debts
(
    debt_id   INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    amount    INT(10) UNSIGNED,
    enabled   BOOLEAN,
    create_at DATETIME,
    update_at DATETIME
);
